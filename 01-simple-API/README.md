### Simple AWS Chalice API 

This API is a sample API, run 
`chalice deploy --stage dev`
To deploy the API

To delete the API, run
`chalice delete --stage dev`
