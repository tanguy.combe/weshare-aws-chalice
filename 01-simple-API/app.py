from chalice import Chalice

app = Chalice(app_name='pingpong')


@app.route('/ping')
def index():
    return {'Response': 'pong'}

@app.route('/xping/{occ}')
def index(occ):
    return {'Response': ''.join('pong' for x in range(0, int(occ)))}

# @app.on_cw_event({"source": ["aws.codecommit"]})
# def on_code_commit_changes(event):
#     print(event.to_dict())

# @app.on_s3_event(bucket='bucketname',
#                  events=['s3:ObjectCreated:*'])
# def handle_s3_event(event):
#     app.log.debug("Received event for bucket: %s, key: %s",
#                   event.bucket, event.key)

# @app.on_sns_message(topic='weshare-topic')
# def handle_sns_message(event):
#     app.log.debug("Received message with subject: %s, message: %s",
#                   event.subject, event.message)