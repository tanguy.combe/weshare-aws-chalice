resource "aws_iam_role" "api_role" {
  name                 = "api-role"

  assume_role_policy = file("${path.module}/policies/lambda-assume-role.json")
}

resource "aws_iam_role_policy_attachment" "api_role_logs" {
  role       = aws_iam_role.api_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy_document" "api_role_policy" {
  statement {
    actions   = [
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:UpdateItem",
      "dynamodb:Scan",
      "dynamodb:Query"
    ]
    resources = [
      aws_dynamodb_table.users_table.arn,
      aws_dynamodb_table.activity_logs.arn
    ]
  }
  statement {
    actions   = [
      "secretsmanager:GetSecretValue"
    ]
    resources = [
      aws_secretsmanager_secret.api_secret.arn
    ]
  }
}

resource "aws_iam_policy" "api_role_policy" {
  name        = "api-policy"

  policy = data.aws_iam_policy_document.api_role_policy.json
}

resource "aws_iam_role_policy_attachment" "api_role" {
  role       = aws_iam_role.api_role.name
  policy_arn = aws_iam_policy.api_role_policy.arn
}

resource "random_string" "api_secret_string" {
  length           = 10
  special          = true
  override_special = "-+{}[]#<>!?._"
}

resource "aws_secretsmanager_secret" "api_secret" {
  name                    = "api-secret"
  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "api_secret_version" {
  secret_id     = aws_secretsmanager_secret.api_secret.id
  secret_string = base64encode(random_string.api_secret_string.result)
}


# DynamoDB Tables
resource "aws_dynamodb_table" "users_table" {
  name         = "users"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "username"

  attribute {
    name = "username"
    type = "S"
  }
}

resource "aws_dynamodb_table" "activity_logs" {
  name         = "activity-logs"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "username"
  range_key    = "timestamp"

  attribute {
    name = "username"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "S"
  }
}