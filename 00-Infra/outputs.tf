output "chalice_iam_role_arn" {
  description = ""
  value       = aws_iam_role.api_role.arn
}

output "jwt_secret_arn" {
  description = ""
  value       = aws_secretsmanager_secret.api_secret.arn
}

output "dynamodb_users_table" {
  description = ""
  value       = aws_dynamodb_table.users_table.name
}

output "dynamodb_user_activity" {
  description = ""
  value       = aws_dynamodb_table.activity_logs.name
}