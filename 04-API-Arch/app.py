import yaml

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from chalicelib.utils.apispec import CustomChalicePlugin
from chalice import Chalice
from chalicelib.auth.routes import *
from chalicelib.login.routes import *
from chalicelib.login.schemas import *
from chalicelib.middleware.routes import *
from chalicelib.pingpong.routes import *
from chalicelib.users.routes import *

app = Chalice(app_name='arch-api')

# REGISTERING
app.register_blueprint(auth_route)
app.register_blueprint(login_routes)
app.register_blueprint(middleware_routes)
app.register_blueprint(pingpong_routes)
app.register_blueprint(users_routes)


OPENAPI_SPEC = f"""
openapi: 3.0.2
info:
  description: Server API document
  title: Swagger Skynet Alerting
  version: 1.0.0
servers:
- url: ""
  description: API server
"""

settings = yaml.safe_load(OPENAPI_SPEC)
# retrieve  title, version, and openapi version
title = settings["info"].pop("title")
spec_version = settings["info"].pop("version")
openapi_version = settings.pop("openapi")
spec = APISpec(
    title=title,
    version=spec_version,
    openapi_version=openapi_version,
    plugins=[MarshmallowPlugin(), CustomChalicePlugin()],
    **settings
)

api_key_scheme = {"type": "apiKey", "in": "header", "name": "Authorization"}
spec.components.security_scheme("ApiKeyAuth", api_key_scheme)
spec.components.schema("LoginSchema", schema=LoginSchema)

spec.path(path='/login', func=log_user)


@app.route('/api', methods=['GET'], cors=True)
def api():
    return spec.to_dict()