import os

from chalice import AuthResponse, Blueprint
from chalicelib.auth.controller import get_auth_key, decode_jwt_token

auth_route = Blueprint(__name__)

@auth_route.authorizer()
def auth(auth_request):
    token = auth_request.token
    decoded = decode_jwt_token(token, get_auth_key())
    return AuthResponse(routes=['*'], principal_id=decoded['sub'])