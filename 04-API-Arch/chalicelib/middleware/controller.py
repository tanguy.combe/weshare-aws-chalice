import json
import time 
import os
import boto3

def log_action(request, result, error_message):
    """This function will save in dynamoDB any action performed by a user

    Args:
        request (Chalice.request): Action performed
        result (str): can be an error or a success
        error_message (str): error message if error occurs
    """
    if request.to_dict()['method'] != "GET":
        try:
            body = request.json_body
        except Exception as error:
            print("body is not well formatted")
            body = {"content": request.raw_body.decode()}
            result = "error"
            error_message = str(error)
        logged_body = {}
        if type(body) == dict:
            if "password" in body.keys():
                username = body['username']
                for k, v in body.items():
                    logged_body[k]=v
                    if k == "password":
                        logged_body[k]="HIDDEN_DUE_TO_SECURITY_REASON"
            else:
                username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
                logged_body = body
        else:
            username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
            logged_body = body
    else:
        username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
        logged_body = {}
    log_data = {
        "username" : username, #PK
        "timestamp" : str(time.time()).replace('.', ''), # SK
        "method" : request.to_dict()['method'],
        "action" : request.to_dict()['path'],
        "uri_params" : request.to_dict().get('uri_params'),
        "query_params" : request.to_dict().get('query_params'),
        "body" : logged_body,
        "result" : result,
        "error_message" : error_message if error_message else ""
    }
    log_table = boto3.resource('dynamodb').Table(os.environ['LOGS_TABLE'])
    log_table.put_item(Item=log_data)
    print(f"Logging Action : {json.dumps(log_data)}")