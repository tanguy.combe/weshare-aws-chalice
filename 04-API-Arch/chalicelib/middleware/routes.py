from chalice import Blueprint, Response

from chalicelib.middleware.controller import log_action

middleware_routes = Blueprint(__name__)

@middleware_routes.middleware('http')
def app_middleware_handler(event, get_response):
    try:
        response = get_response(event)
        if event.to_dict()['path'] == "/api":
            return response
        log_action(event, result="success", error_message="")
        return response
    except Exception as e:
        log_action(event, result="error", error_message=str(e))
        return Response(status_code=400, body={'Result': 'error', 'Message': str(e)},
                        headers={'Content-Type': 'application/json'})