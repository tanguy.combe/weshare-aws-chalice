from chalice import Blueprint
from chalicelib.users.controller import encode_password, USERS_TABLE
from boto3.dynamodb.types import Binary

users_routes = Blueprint(__name__)

@users_routes.route('/users/new', methods=['POST'], content_types=['application/json'])
def add_user():
    body = users_routes.current_request.json_body
    password_fields = encode_password(body['password'])
    ddb_item = {
        'username': body['username'],
        'hash': password_fields['hash'],
        'salt': Binary(password_fields['salt']),
        'rounds': password_fields['rounds'],
        'hashed': Binary(password_fields['hashed']),
    }
    USERS_TABLE.put_item(Item=ddb_item)
    return {'username': body['username'], 'status': 'created'}