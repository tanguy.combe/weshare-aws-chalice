import os
import hashlib
import boto3

USERS_TABLE = boto3.resource('dynamodb').Table(os.environ['USERS_TABLE'])

def encode_password(password, salt=None):
    if salt is None:
        salt = os.urandom(32)
    rounds = 100000
    hashed = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'),
                                 salt, rounds)
    return {
        'hash': 'sha256',
        'salt': salt,
        'rounds': rounds,
        'hashed': hashed,
    }

