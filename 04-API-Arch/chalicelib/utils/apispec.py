from apispec import BasePlugin, yaml_utils

class CustomChalicePlugin(BasePlugin):
    def path_helper(self, path, operations, func, **kwargs):
        operations.update(yaml_utils.load_operations_from_docstring(func.__doc__))
        return path