from chalice import Blueprint
from chalicelib.pingpong.controller import ping_request
from chalicelib.auth.routes import auth

pingpong_routes = Blueprint(__name__)

@pingpong_routes.route('/ping')
def index():
    return ping_request()

@pingpong_routes.route('/auth_ping', authorizer=auth)
def index():
    return ping_request()