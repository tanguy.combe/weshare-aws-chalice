from chalice import Blueprint, ChaliceUnhandledError
from chalicelib.login.controller import login

login_routes = Blueprint(__name__)

@login_routes.route('/login', methods=['POST'], content_types=['application/json'])
def log_user():
    """
    ---
    post:
      description: Return JWT against valid username and password 
      requestBody:
          content:
            application/json:
              schema: LoginSchema
      responses:
        200:
          description: POST response
          content:
            application/json:
              schema:
                type: object
                properties:
                  token:
                    type: string
    """
    try:
        return login(login_routes.current_request)
    except Exception as error:
        raise ChaliceUnhandledError(error)