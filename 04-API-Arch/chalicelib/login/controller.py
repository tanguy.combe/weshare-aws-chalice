from chalicelib.users.controller import USERS_TABLE
from chalicelib.auth.controller import get_auth_key, get_jwt_token
from chalicelib.login.schemas import LoginSchema


def login(request):
    """this function will log user and retrieve usename and RBAC infos
    """
    body = request.json_body
    LoginSchema().load(body)
    resp = USERS_TABLE.get_item(
        Key={'username': body['username']})
    record = resp.get('Item')
    if not record:
        return {
            'Code' : 'NotFoundError',
            'Message' : 'User not found'
        }
    jwt_token = get_jwt_token(
        body['username'], body['password'], record, get_auth_key())
    return {'token': jwt_token, 'username': body['username']}