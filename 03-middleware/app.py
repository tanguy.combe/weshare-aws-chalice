import boto3
import os
import base64
import jwt
import hashlib
import hmac
import datetime
import json
import time

from uuid import uuid4
from chalice import Chalice, AuthResponse, UnauthorizedError, Response
from boto3.dynamodb.types import Binary

USERS_TABLE = boto3.resource('dynamodb').Table(os.environ['USERS_TABLE'])

app = Chalice(app_name='middleware-api')

# UTILS
####################################################################
def get_auth_key():
    """This function get the key needed to decrypt password and tokens in Secrets manager
    """
    client = boto3.client(
        service_name='secretsmanager'
    )
    get_secret_value_response = client.get_secret_value(
        SecretId=os.environ['AUTH_KEY']
    )
    return base64.b64decode(get_secret_value_response['SecretString'])

def get_jwt_token(username, password, record, secret):
    actual = hashlib.pbkdf2_hmac(
        record['hash'],
        password.encode('utf-8'),
        record['salt'].value,
        record['rounds']
    )
    expected = record['hashed'].value
    if hmac.compare_digest(actual, expected):
        now = datetime.datetime.utcnow()
        unique_id = str(uuid4())
        payload = {
            'sub': username,
            'iat': now,
            'nbf': now,
            'jti': unique_id,
            # NOTE: We can also add 'exp' if we want tokens to expire.
        }
        return jwt.encode(payload, secret, algorithm='HS256').decode('utf-8')
    raise UnauthorizedError('Invalid password')

def decode_jwt_token(token, secret):
    return jwt.decode(token, secret, algorithms=['HS256'])

def encode_password(password, salt=None):
    if salt is None:
        salt = os.urandom(32)
    rounds = 100000
    hashed = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'),
                                 salt, rounds)
    return {
        'hash': 'sha256',
        'salt': salt,
        'rounds': rounds,
        'hashed': hashed,
    }

def log_action(request, result, error_message):
    """This function will save in dynamoDB any action performed by a user

    Args:
        request (Chalice.request): Action performed
        result (str): can be an error or a success
        error_message (str): error message if error occurs
    """
    if request.to_dict()['method'] != "GET":
        try:
            body = request.json_body
        except Exception as error:
            print("body is not well formatted")
            body = {"content": request.raw_body.decode()}
            result = "error"
            error_message = str(error)
        logged_body = {}
        if type(body) == dict:
            if "password" in body.keys():
                username = body['username']
                for k, v in body.items():
                    logged_body[k]=v
                    if k == "password":
                        logged_body[k]="HIDDEN_DUE_TO_SECURITY_REASON"
            else:
                username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
                logged_body = body
        else:
            username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
            logged_body = body
    else:
        username = request.to_dict()['context'].get('authorizer', {}).get('principalId', "unauth.user")
        logged_body = {}
    log_data = {
        "username" : username, #PK
        "timestamp" : str(time.time()).replace('.', ''), # SK
        "method" : request.to_dict()['method'],
        "action" : request.to_dict()['path'],
        "uri_params" : request.to_dict().get('uri_params'),
        "query_params" : request.to_dict().get('query_params'),
        "body" : logged_body,
        "result" : result,
        "error_message" : error_message if error_message else ""
    }
    log_table = boto3.resource('dynamodb').Table(os.environ['LOGS_TABLE'])
    log_table.put_item(Item=log_data)
    print(f"Logging Action : {json.dumps(log_data)}")

# AUTHORIZER
####################################################################
@app.authorizer()
def auth(auth_request):
    token = auth_request.token
    decoded = decode_jwt_token(token, get_auth_key())
    return AuthResponse(routes=['*'], principal_id=decoded['sub'])

# MIDDLEWARE
####################################################################
@app.middleware('http')
def app_middleware_handler(event, get_response):
    try:
        response = get_response(event)
        if event.to_dict()['path'] == "/api":
            return response
        log_action(event, result="success", error_message="")
        return response
    except Exception as e:
        log_action(event, result="error", error_message=str(e))
        return Response(status_code=400, body={'Result': 'error', 'Message': str(e)},
                        headers={'Content-Type': 'application/json'})


# ROUTES
####################################################################
@app.route('/ping')
def index():
    return {'Response': 'pong'}

@app.route('/auth_ping', authorizer=auth)
def index():
    return {'Response': 'pong'}

@app.route('/new/user', methods=['POST'], content_types=['application/json'])
def create_user():
    body = app.current_request.json_body
    password_fields = encode_password(body['password'])
    ddb_item = {
        'username': body['username'],
        'hash': password_fields['hash'],
        'salt': Binary(password_fields['salt']),
        'rounds': password_fields['rounds'],
        'hashed': Binary(password_fields['hashed']),
    }
    USERS_TABLE.put_item(Item=ddb_item)
    return {'username': body['username'], 'status': 'created'}
    
@app.route('/login', methods=['POST'], content_types=['application/json'])
def login():
    body = app.current_request.json_body
    resp = USERS_TABLE.get_item(
        Key={'username': body['username']})
    record = resp.get('Item')
    if not record:
        return {
            'Code' : 'NotFoundError',
            'Message' : 'User not found'
        }
    jwt_token = get_jwt_token(
        body['username'], body['password'], record, get_auth_key())
    return {'token': jwt_token, 'username': body['username']}