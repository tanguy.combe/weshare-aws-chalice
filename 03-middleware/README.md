### middleware AWS Chalice API 

This API is an middleware API, run 
`chalice deploy --stage dev`
To deploy the API

To delete the API, run
`chalice delete --stage dev`

curl -X GET https://yaso0gjb38.execute-api.eu-west-1.amazonaws.com/dev/ping

curl -X POST https://yaso0gjb38.execute-api.eu-west-1.amazonaws.com/dev/new/user \
   -H 'Content-Type: application/json' \
   -d '{"username":"guy.tarembois","password":"1234"}'

curl -X POST https://yaso0gjb38.execute-api.eu-west-1.amazonaws.com/dev/login \
   -H 'Content-Type: application/json' \
   -d '{"username":"guy.tarembois","password":"1234"}'

curl -X GET https://yaso0gjb38.execute-api.eu-west-1.amazonaws.com/dev/auth_ping

curl -X GET https://yaso0gjb38.execute-api.eu-west-1.amazonaws.com/dev/auth_ping \
   -H 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJndXkudGFyZW1ib2lzIiwiaWF0IjoxNjUxNDAxNTYyLCJuYmYiOjE2NTE0MDE1NjIsImp0aSI6Ijc3NzFkN2JhLWM1ZjMtNDRhMC04YmI1LWYxZTIxMTJiNzcyYSJ9.iorDkjmXUxLqeEzdBVYuQoUwPq4kUD5tg37X_IQ2EOg'